package com.absa.billing.upload.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Document {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	protected Long id;

	private String name;

	private String downloadUri;

	private String type;

	private long size;

	public Document() {
		super();
	}

	public Document(String name, String downloadUri, String type, long size) {
		super();
		this.name = name;
		this.downloadUri = downloadUri;
		this.type = type;
		this.size = size;
	}

	public Document(Long id, String name, String downloadUri, String type, long size) {
		this(name, downloadUri, type, size);
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDownloadUri() {
		return downloadUri;
	}

	public void setDownloadUri(String downloadUri) {
		this.downloadUri = downloadUri;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}
}
