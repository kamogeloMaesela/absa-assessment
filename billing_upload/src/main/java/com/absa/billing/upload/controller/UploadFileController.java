package com.absa.billing.upload.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.absa.billing.upload.service.UploadService;

@RestController
@RequestMapping("/api")
public class UploadFileController {

	private UploadService uploadService;

	@Autowired
	public UploadFileController(UploadService uploadService) {
		this.uploadService = uploadService;
	}

	@PostMapping(value = "/upload")
	public boolean uploadFile(@RequestParam("docs") MultipartFile multipartFile) {
		try {
			uploadService.saveFile(multipartFile);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

}
