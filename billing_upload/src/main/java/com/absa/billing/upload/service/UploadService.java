package com.absa.billing.upload.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface UploadService {

	void saveFile(MultipartFile multipartFile) throws IOException;

}
