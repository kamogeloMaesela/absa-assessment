package com.absa.billing.upload.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.absa.billing.upload.FileStorageProperties;

@Service
@Transactional
public class UploadServiceImpl implements UploadService {

	private final FileStorageProperties fileStorageProperties;

	private final Path fileStorageLocation;

	@Autowired
	public UploadServiceImpl(FileStorageProperties fileStorageProperties) {
		this.fileStorageProperties = fileStorageProperties;
		this.fileStorageLocation = Paths.get(this.fileStorageProperties.getUploadDirectory());
	}

	@Override
	public void saveFile(MultipartFile multipartFile) throws IOException {
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

		Path targetLocation = this.fileStorageLocation.resolve(fileName);
		Files.copy(multipartFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/transaction/uploads")
				.path(fileName).toUriString();
	}

}
