package com.absa.billing.integration.scenario1.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.web.client.ResourceAccessException;

import com.absa.billing.integration.scenario1.resource.MonthTransactionSummaryRepository;
import com.absa.billing.integration.scenario1.resource.TransactionRepository;

public class TransactionServiceTest {

	private TransactionService transactionService;
	private final TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
	private final MonthTransactionSummaryRepository monthTransactionSummaryRepository = Mockito
			.mock(MonthTransactionSummaryRepository.class);

	@Before
	public void setUp() {
		this.transactionService = new TransactionServiceImpl(transactionRepository, monthTransactionSummaryRepository);

	}

	@Test
	public void shouldReturnLastMonthStartDate2013_03_01WhenDateOfSummary2013_04_20IsGiven() {
		// ****************Arrange************//
		LocalDate dateOfSummary = LocalDate.of(2013, 04, 20);
		// *******************Act*************//
		LocalDate startDate = transactionService.getStartDateOfPreviousMonth(dateOfSummary);
		// ****************Assert*************//
		LocalDate expected = LocalDate.of(2013, 03, 1);
		assertEquals(expected, startDate);
	}

	@Test
	public void shouldReturnLastMonthStartDate2019_11_1WhenDateOfSummary2019_12_02IsGiven() {
		// ****************Arrange************//
		LocalDate dateOfSummary = LocalDate.of(2019, 12, 2);
		// *******************Act*************//
		LocalDate startDate = transactionService.getStartDateOfPreviousMonth(dateOfSummary);
		// ****************Assert*************//
		LocalDate expected = LocalDate.of(2019, 11, 1);
		assertEquals(expected, startDate);
	}

	@Test
	public void shouldReturnLastMonthEndDate2019_11_30WhenDateOfSummary2019_12_02IsGiven() {
		// ****************Arrange************//
		LocalDate dateOfSummary = LocalDate.of(2019, 12, 2);
		// *******************Act*************//
		LocalDate startDate = transactionService.getEndDateOfPreviousMonth(dateOfSummary);
		// ****************Assert*************//
		LocalDate expected = LocalDate.of(2019, 11, 30);
		assertEquals(expected, startDate);
	}

	@Test
	public void shouldReturnLastMonthEndDate2018_12_31WhenDateOfSummary2018_01_02IsGiven() {
		// ****************Arrange************//
		LocalDate dateOfSummary = LocalDate.of(2019, 01, 02);
		// *******************Act*************//
		LocalDate startDate = transactionService.getEndDateOfPreviousMonth(dateOfSummary);
		// ****************Assert*************//
		LocalDate expected = LocalDate.of(2018, 12, 31);
		assertEquals(expected, startDate);
	}

	@Test(expected = ResourceAccessException.class)
	public void shouldThrowAnExceWhenGeneratingMonthSammaries() {
		// *******************Act*************//
		transactionService.getMonthBillingStats();

	}

}
