package com.absa.billing.integration.scenario1.service;

import java.time.LocalDate;

public interface TransactionService {

	void getMonthBillingStats();

	public LocalDate getEndDateOfPreviousMonth(LocalDate dateOfSummary);

	LocalDate getStartDateOfPreviousMonth(LocalDate dateOfSummary);

}
