package com.absa.billing.integration.scenario2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SequentialAndParrallelStream {
	/*
	 * just to demonstrate the the sequence of printing is different
	 */
	private static Predicate<? super String> predicate = str -> !str.isEmpty();

	public static void main(String[] args) {

		List<String> statuses = Arrays.asList("Received", "Pending", "", "Awaiting Maturity", "Completed", "");
		printParallelStream(statuses);
		printSequentialStream(statuses);
	}

	/*
	 * execute in the order of the list
	 */
	private static void printSequentialStream(List<String> statuses) {

		System.out.println("**********************************Sequiential**********************");
		List<String> filteredList = statuses.stream().filter(predicate).collect(Collectors.toList());
		filteredList.stream().forEach(System.out::println);
	}

	/*
	 * Its more efficient. Execute in parallel meaning no order of execution
	 */
	private static void printParallelStream(List<String> statuses) {
		System.out.println("**********************************Parallel*****************************");
		List<String> filteredList = statuses.parallelStream().filter(predicate).collect(Collectors.toList());
		filteredList.parallelStream().forEach(System.out::println);
	}

}
