package com.absa.billing.integration.scenario1.resource;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.absa.billing.integration.scenario1.domain.MonthTransactionSummary;

@Repository
public interface MonthTransactionSummaryRepository extends JpaRepository<MonthTransactionSummary, Integer> {

}
