package com.absa.billing.integration.scenario1.domain.base;

import java.time.LocalDate;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private LocalDate dateTimeCreated;

	public BaseEntity() {
	}

	public BaseEntity(Integer id, LocalDate dateTimeCreated) {
		this.id = id;
		this.dateTimeCreated = dateTimeCreated;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getDateTimeCreated() {
		return dateTimeCreated;
	}

	public void setDateTimeCreated(LocalDate dateTimeCreated) {
		this.dateTimeCreated = dateTimeCreated;
	}

}
