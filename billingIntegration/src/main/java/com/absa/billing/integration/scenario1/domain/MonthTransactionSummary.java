package com.absa.billing.integration.scenario1.domain;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.absa.billing.integration.scenario1.domain.base.BaseEntity;

@Entity
public class MonthTransactionSummary extends BaseEntity {

	@Column(name = "service_name")
	private static final String SERVICE_NAME = "INTEGRATEDSERVICES";

	private String clientSwiftAddress;

	private String subService;

	private int usageStats;

	@ManyToMany
	@JoinTable(name = "month_transaction_summary_transaction", joinColumns = @JoinColumn(name = "month_transaction_summary_id"), inverseJoinColumns = @JoinColumn(name = "transaction_id"))
	private List<Transaction> transactions;

	public MonthTransactionSummary() {
	}

	public MonthTransactionSummary(String clientSwiftAddress, String subService, int usageStats,
			List<Transaction> transactions) {
		this.clientSwiftAddress = clientSwiftAddress;
		this.subService = subService;
		this.usageStats = usageStats;
		this.transactions = transactions;
	}

	public MonthTransactionSummary(Integer id, LocalDate dateTimeCreated, String clientSwiftAddress, String subService,
			int usageStats) {
		super(id, dateTimeCreated);
		this.clientSwiftAddress = clientSwiftAddress;
		this.subService = subService;
		this.usageStats = usageStats;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public String getSubService() {
		return subService;
	}

	public void setSubService(String subService) {
		this.subService = subService;
	}

	public int getUsageStats() {
		return usageStats;
	}

	public void setUsageStats(int usageStats) {
		this.usageStats = usageStats;
	}

	public static String getServicename() {
		return SERVICE_NAME;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

}
