package com.absa.billing.integration.scenario1.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.absa.billing.integration.scenario1.domain.base.BaseEntity;

@Entity
public class Transaction extends BaseEntity implements Comparable<Transaction> {

	public enum MessageStatus {
		COMPLETED, REJECTED
	}

	private String transactionReference;

	private String clientSwiftAddress;

	@Enumerated(EnumType.STRING)
	private MessageStatus messageStatus;

	private String currency;

	private double amount;

	public Transaction() {
		super();
	}

	public Transaction(Integer id, String transactionReference, String clientSwiftAddress, MessageStatus messageStatus,
			String currency, double amount, LocalDate dateTimeCreated) {
		super(id, dateTimeCreated);
		this.transactionReference = transactionReference;
		this.clientSwiftAddress = clientSwiftAddress;
		this.messageStatus = messageStatus;
		this.currency = currency;
		this.amount = amount;
	}

	public Transaction(String transactionReference, String clientSwiftAddress, MessageStatus messageStatus,
			String currency, double amount, LocalDate dateTimeCreated) {
		this(null, transactionReference, clientSwiftAddress, messageStatus, currency, amount, dateTimeCreated);
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public MessageStatus getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(MessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientSwiftAddress == null) ? 0 : clientSwiftAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (clientSwiftAddress == null) {
			if (other.clientSwiftAddress != null)
				return false;
		} else if (!clientSwiftAddress.equals(other.clientSwiftAddress))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + getId() + ", transactionReference=" + transactionReference + ", clientSwiftAddress="
				+ clientSwiftAddress + ", messageStatus=" + messageStatus + ", currency=" + currency + ", amount="
				+ amount + ", dateTimeCreated=" + getDateTimeCreated() + "]";
	}

	@Override
	public int compareTo(Transaction transaction) {
		return this.getCurrency().compareTo(transaction.getCurrency());
	}

}
