package com.absa.billing.integration.scenario1.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.absa.billing.integration.scenario1.domain.MonthTransactionSummary;
import com.absa.billing.integration.scenario1.domain.Transaction;
import com.absa.billing.integration.scenario1.domain.Transaction.MessageStatus;
import com.absa.billing.integration.scenario1.resource.MonthTransactionSummaryRepository;
import com.absa.billing.integration.scenario1.resource.TransactionRepository;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	private static final String UPLOAD_API_URL = "http://localhost:8081/api/upload";

	private static final String CURRENCY_EXTENSION = "OUT";

	private static final String OTHER_CURRENCY = "CCY";

	private static final String SA_CURRENCY_CODE = "ZAR";

	private final TransactionRepository transactionRepository;

	private final MonthTransactionSummaryRepository monthTransactionSummaryRepository;

	@Autowired
	public TransactionServiceImpl(TransactionRepository transactionRepository,
			MonthTransactionSummaryRepository monthTransactionSummaryRepository) {
		this.transactionRepository = transactionRepository;
		this.monthTransactionSummaryRepository = monthTransactionSummaryRepository;
	}

	@Override
	@Scheduled(cron = "00 00 7 1 * 1-5")
	public void getMonthBillingStats() {
		final LocalDate dateOfSummary = LocalDate.now();

		LocalDate startOfMonth = getStartDateOfPreviousMonth(dateOfSummary);
		LocalDate endOfMonth = getEndDateOfPreviousMonth(dateOfSummary);

		List<Transaction> SATransactions = transactionRepository
				.findByMessageStatusAndCurrencyAndDateTimeCreatedBetween(MessageStatus.COMPLETED, SA_CURRENCY_CODE,
						startOfMonth, endOfMonth);

		List<Transaction> otherTransactions = transactionRepository
				.findByMessageStatusAndCurrencyNotAndDateTimeCreatedBetween(MessageStatus.COMPLETED, SA_CURRENCY_CODE,
						startOfMonth, endOfMonth);
		Map<String, List<Transaction>> groupedSATransactions = groupTransactionsByClientSwiftAddres(SATransactions);
		Map<String, List<Transaction>> groupedOtherTransactions = groupTransactionsByClientSwiftAddres(
				otherTransactions);
		List<MonthTransactionSummary> monthTransactionSummaries = getTransactionSummaries(groupedSATransactions,
				dateOfSummary);
		monthTransactionSummaries.addAll(getTransactionSummaries(groupedOtherTransactions, dateOfSummary));
		monthTransactionSummaryRepository.saveAll(monthTransactionSummaries);

		FileSystemResource fileSystemResource = createResource(monthTransactionSummaries);
		ResponseEntity<String> response = uploadFileToApi(fileSystemResource);

		if (!response.getBody().equalsIgnoreCase("true")) {
			throw new RuntimeException("Failed to upload file to the BillingAploadApi");
		}
	}

	@Override
	public LocalDate getEndDateOfPreviousMonth(LocalDate dateOfSummary) {
		LocalDate lastMonth = dateOfSummary.minusMonths(1);
		return LocalDate.of(lastMonth.getYear(), lastMonth.getMonth(), lastMonth.getMonth().maxLength());
	}

	@Override
	public LocalDate getStartDateOfPreviousMonth(LocalDate dateOfSummary) {
		LocalDate lastMonth = dateOfSummary.minusMonths(1);
		return LocalDate.of(lastMonth.getYear(), lastMonth.getMonth(), 1);
	}

	private ResponseEntity<String> uploadFileToApi(FileSystemResource fileSystemResource) {
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("docs", fileSystemResource);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.postForEntity(UPLOAD_API_URL, requestEntity, String.class);
	}

	private List<MonthTransactionSummary> getTransactionSummaries(Map<String, List<Transaction>> groupedTransactions,
			final LocalDate dateOfSummary) {
		List<MonthTransactionSummary> monthTransactionSummaries = new ArrayList<>();
		for (Map.Entry<String, List<Transaction>> entry : groupedTransactions.entrySet()) {
			List<Transaction> transactions = entry.getValue();
			MonthTransactionSummary summary = new MonthTransactionSummary(entry.getKey(),
					derternineSubService(transactions), transactions.size(), transactions);
			summary.setDateTimeCreated(dateOfSummary);
			monthTransactionSummaries.add(summary);
		}
		return monthTransactionSummaries;
	}

	private String getSummaryLine(MonthTransactionSummary summary) {
		final String start = "%-";
		final String end = "s";
		String Line = String.format(start + 18 + end, MonthTransactionSummary.getServicename());
		Line += String.format(start + 11 + end, summary.getClientSwiftAddress());
		Line += String.format(start + 6 + end, summary.getSubService());
		Line += String.format(start + 8 + end, summary.getDateTimeCreated());
		Line += String.format(start + 6 + end, summary.getUsageStats());
		return Line;
	}

	private String derternineSubService(List<Transaction> transactions) {
		if (transactions.get(0).getCurrency().equals(SA_CURRENCY_CODE))
			return SA_CURRENCY_CODE + CURRENCY_EXTENSION;
		return OTHER_CURRENCY + CURRENCY_EXTENSION;
	}

	private Map<String, List<Transaction>> groupTransactionsByClientSwiftAddres(List<Transaction> transactions) {
		Map<String, List<Transaction>> groppedBySiftAddresTransactions = transactions.stream()
				.collect(Collectors.groupingBy(transaction -> transaction.getClientSwiftAddress()));
		return groppedBySiftAddresTransactions;

	}

	private FileSystemResource createResource(List<MonthTransactionSummary> monthTransactionSummaries) {
		String fileName = "src/main/resources/Billing_" + LocalDate.now().toString().replaceAll("[.:]", "_") + ".txt";
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");
			for (MonthTransactionSummary summary : monthTransactionSummaries) {
				writer.println(getSummaryLine(summary));
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		return new FileSystemResource(new File(fileName));
	}
}
