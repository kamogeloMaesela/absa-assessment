package com.absa.billing.integration.scenario1.resource;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.absa.billing.integration.scenario1.domain.Transaction;
import com.absa.billing.integration.scenario1.domain.Transaction.MessageStatus;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	List<Transaction> findByMessageStatusAndCurrencyAndDateTimeCreatedBetween(MessageStatus messageStatus,
			String currency, LocalDate dateTimeCreatedStart, LocalDate dateTimeCreatedEnd);

	List<Transaction> findByMessageStatusAndCurrencyNotAndDateTimeCreatedBetween(MessageStatus messageStatus,
			String currency, LocalDate dateTimeCreatedStart, LocalDate dateTimeCreatedEnd);

	List<Transaction> findByMessageStatusAndDateTimeCreatedBetween(MessageStatus messageStatus,
			LocalDate dateTimeCreatedStart, LocalDate dateTimeCreatedEnd);

}
